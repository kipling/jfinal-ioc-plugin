# jfinal-ioc-plugin
JFinal 依赖注入插件

**1.在JFinalConfig文件中配置IOC插件**

```java
@Override
public void configConstant(Constants me) {

	// 创建controller实现依赖注入
	me.setControllerFactory(new IocControllerFactory());
}

@Override
public void configPlugin(Plugins me) {
	// 扫描包名目录 
	String basePackage = "com.kipling.jfinal.plugin";
	me.add(new IocPlugin(basePackage));

}
```
**2.Controller 中的Service 添加注释JFAutowired**
 
```java

public class TestController extends Controller{
	
	@JFAutowired
	TestJFService jfService;
	
	public void test() {
		System.out.println("TestController.test");
		renderText(jfService.test());
	}
}
```

**3.Service类添加JFService注释**

```java

@JFService
public class TestJFService {
	
	public String  test() {
		System.out.println("I am TestJFService.test()");
		return "Hello This is JFService.";
	}
	
}
```
