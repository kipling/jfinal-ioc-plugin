package com.kipling.jfinal.common;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.template.Engine;
import com.kipling.jfinal.plugin.ioc.IocPlugin;
import com.kipling.jfinal.plugin.ioc.core.IocControllerFactory;
import com.kipling.jfinal.plugin.ioc.test.TestController;

public class JFIocConfig extends JFinalConfig {
	
	public static void main(String[] args) {
		/**
		 * 特别注意：Eclipse 之下建议的启动方式
		 */
		JFinal.start("src/main/webapp", 80, "/", 5);
		
	}

	@Override
	public void configConstant(Constants me) {
		me.setControllerFactory(new IocControllerFactory());
	}

	@Override
	public void configRoute(Routes me) {
		me.add("/index",TestController.class);
	}

	@Override
	public void configEngine(Engine me) {
		
	}

	@Override
	public void configPlugin(Plugins me) {
		String basePackage = "com.kipling.jfinal.plugin";
		me.add(new IocPlugin(basePackage));

	}

	@Override
	public void configInterceptor(Interceptors me) {
		// TODO Auto-generated method stub

	}

	@Override
	public void configHandler(Handlers me) {
		// TODO Auto-generated method stub

	}

}
