package com.kipling.jfinal.plugin.ioc.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kipling.jfinal.plugin.ioc.annotation.JFBean;
import com.kipling.jfinal.plugin.ioc.annotation.JFService;

/**
 * 
 * @author kipling
 *
 */
public class BeanLoader {
	
	/**
     * Bean Map（Bean 类 => Bean 实例）
     */
    private static final Map<Class<?>, Object> beanMap = new HashMap<Class<?>, Object>();
    static {
        try {
            // 获取应用包路径下所有的类
            List<Class<?>> classList = KClassLoader.getInstance().getClassList();
            for (Class<?> cls : classList) {
                // 处理带有 JFService/JFBean 注解的类
                if (cls.isAnnotationPresent(JFService.class) ||
                    cls.isAnnotationPresent(JFBean.class) ) {
                    // 创建 Bean 实例
                    Object beanInstance = cls.newInstance();
                    // 将 Bean 实例放入 Bean Map 中（键为 Bean 类，值为 Bean 实例）
                    beanMap.put(cls, beanInstance);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("初始化 BeanLoader 出错！", e);
        }
    }
    
    
    /**
     * 获取 Bean Map
     */
    public static Map<Class<?>, Object> getBeanMap() {
        return beanMap;
    }

}
