package com.kipling.jfinal.plugin.ioc.core;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import com.kipling.jfinal.plugin.ioc.core.impl.DefaultClassScanner;
import com.kipling.jfinal.plugin.ioc.util.ObjectUtil;

/**
 * 实例工厂
 *
 * @author huangyong
 */
public class InstanceFactory {
	
	 /**
     * 用于缓存对应的实例
     */
    private static final Map<String, Object> cache = new ConcurrentHashMap<String, Object>();
    /**
     * ClassScanner
     */
    private static final String CLASS_SCANNER = "jfinal.plugin.ioc.class_scanner";
    
    /**
     * 获取 ClassScanner
     */
    public static ClassScanner getClassScanner() {
        return getInstance(CLASS_SCANNER, DefaultClassScanner.class);
    }
    
    @SuppressWarnings("unchecked")
    public static <T> T getInstance(String cacheKey, Class<T> defaultImplClass) {
        // 若缓存中存在对应的实例，则返回该实例
        if (cache.containsKey(cacheKey)) {
            return (T) cache.get(cacheKey);
        }
        // 从配置文件中获取相应的接口实现类配置
        String implClassName = defaultImplClass.getName();
        // 通过反射创建该实现类对应的实例
        T instance = ObjectUtil.newInstance(implClassName);
        // 若该实例不为空，则将其放入缓存
        if (instance != null) {
            cache.put(cacheKey, instance);
        }
        // 返回该实例
        return instance;
    }

}
