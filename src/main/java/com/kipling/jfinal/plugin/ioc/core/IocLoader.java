package com.kipling.jfinal.plugin.ioc.core;

import java.lang.reflect.Field;
import java.util.Map;
import com.kipling.jfinal.plugin.ioc.annotation.JFAutowired;

/**
 * 
 * @author kipling
 *
 */
public class IocLoader {
	
	 static {
	        try {
	        	 // 获取并遍历所有的 Bean 类
	            Map<Class<?>, Object> beanMap = BeanLoader.getBeanMap();
	            for (Map.Entry<Class<?>, Object> beanEntry : beanMap.entrySet()) {
	                // 获取 Bean 类与 Bean 实例
	                Class<?> beanClass = beanEntry.getKey();
	                Object beanInstance = beanEntry.getValue();
	                // 获取 Bean 类中所有的字段（不包括父类中的方法）
	                Field[] beanFields = beanClass.getDeclaredFields();
	                
	                for (Field beanField : beanFields) {
                        // 判断当前 Bean 字段是否带有 Inject 注解
                        if (beanField.isAnnotationPresent(JFAutowired.class)) {
                            // 获取 Bean 字段对应的类
                            Class<?> implementClass = beanField.getType();
                            // 若存在实现类，则执行以下代码
                            if (implementClass != null) {
                                // 从 Bean Map 中获取该实现类对应的实现类实例
                                Object implementInstance = beanMap.get(implementClass);
                                // 设置该 Bean 字段的值
                                if (implementInstance != null) {
                                    beanField.setAccessible(true); // 将字段设置为 public
                                    beanField.set(beanInstance, implementInstance); // 设置字段初始值
                                } else {
                                    throw new RuntimeException("依赖注入失败！类名：" + beanClass.getSimpleName() + "，字段名：" + implementClass.getSimpleName());
                                }
                            }
                        }
                    }
	            }
	        }catch (Exception e) {
	            throw new RuntimeException("初始化 IocLoader 出错！", e);
	        }
	 }
	 
	 

}
