package com.kipling.jfinal.plugin.ioc.core;

import java.lang.annotation.Annotation;
import java.util.List;

import com.jfinal.kit.LogKit;
import com.jfinal.kit.StrKit;

import com.kipling.jfinal.plugin.ioc.util.ClassUtil;

/**
 * 
 * @author kipling
 *
 */
public class KClassLoader {
	
	private static KClassLoader instance;
	
	private boolean initialized = false;
	String basePackage= "";
	
	public static synchronized void init(String basePackage) {
		if(instance==null) {
			instance = new KClassLoader(basePackage);
		}
	}
	
	public static KClassLoader getInstance() {
		return instance;
	}
	public KClassLoader(String basePackage) {
		
		if(StrKit.isBlank(basePackage)) {
			LogKit.error("包名不能为空。");
			throw new RuntimeException("包名不能为空。");
			
		}
		this.basePackage = basePackage;		
		initialized = true;
	}
	
	public boolean classLoader() {
		Class<?>[] classList = {BeanLoader.class,IocLoader.class};
		for (Class<?> cls : classList) {
			ClassUtil.loadClass(cls.getName());
		}
		return true;
	}	
	
	/**
     * 获取 ClassScanner
     */
    private static final ClassScanner classScanner = InstanceFactory.getClassScanner();

    /**
     * 获取基础包名中的所有类
     */
    public List<Class<?>> getClassList() {
        return classScanner.getClassList(basePackage);
    }

    /**
     * 获取基础包名中指定父类或接口的相关类
     */
    public  List<Class<?>> getClassListBySuper(Class<?> superClass) {
        return classScanner.getClassListBySuper(basePackage, superClass);
    }

    /**
     * 获取基础包名中指定注解的相关类
     */
    public List<Class<?>> getClassListByAnnotation(Class<? extends Annotation> annotationClass) {
        return classScanner.getClassListByAnnotation(basePackage, annotationClass);
    }
	public boolean isInitialized() {
		return initialized;
	}
	public void setInitialized(boolean initialized) {
		this.initialized = initialized;
	}
	
    
}
