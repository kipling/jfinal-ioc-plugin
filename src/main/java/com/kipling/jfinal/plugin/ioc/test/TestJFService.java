package com.kipling.jfinal.plugin.ioc.test;

import com.kipling.jfinal.plugin.ioc.annotation.JFService;

@JFService
public class TestJFService {
	
	public String  test() {
		System.out.println("I am TestJFService.test()");
		return "Hello This is JFService.";
	}
	
}
